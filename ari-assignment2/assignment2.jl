### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 4ba27e20-cc90-11ec-2be1-077b0b284b2d
using PlutoUI

# ╔═╡ 88e93662-6def-48d3-a58b-5706867e9dc7
using DataStructures

# ╔═╡ 883d9b0c-2126-4f01-9c84-2fc5c40542f0
 using HypertextLiteral: @htl

# ╔═╡ 2e76891f-c5fb-44df-9129-48e90a5e334d
using StructArrays

# ╔═╡ 585c014d-fcc9-4a39-bc17-23faf9823262
md"""# The Setup"""

# ╔═╡ bb050b2c-b2c4-41fa-86ee-157735218c85
function user_inputs(input)
	PlutoUI.combine() do Child
		@htl("""
		<h6>Enter number of floors and offices in the Company.</h6>
		<ul>
		$([
			@htl("<li>$(name): $(Child(name, Slider(1:50)))</li>")
			for name in input
		])
		</ul>
		
		""")
	end
end


# ╔═╡ 191af719-c27d-45bb-9add-1fa19d9f2bb4
@bind list_input user_inputs(["floors", "offices","parcels"])


# ╔═╡ 4814db00-a3d3-40d8-863e-d4c311cb608b
with_terminal() do
 print(list_input)
end

# ╔═╡ 3abbfc16-5b08-4a2d-a54d-20b34fc53d11
function location(loc)
	PlutoUI.combine() do Child
		@htl("""
		<h6>Enter location of agent.</h6>
		<ul>
		$([
			@htl("<li>$(name): $(Child(name, Slider(1:10)))</li>")
			for name in loc
		])
		</ul>
		
		""")
	end
end

# ╔═╡ e283ac78-5272-4c37-ae0c-ed28794e9bd9
@bind loc location(["floor", "office"])

# ╔═╡ 56d5aa2c-9ed6-40a3-b4e0-eb4378a9b4ef
num_floor =list_input[1] 

# ╔═╡ 14d02599-537b-419c-ad1b-119735fe515a
num_office = list_input[2]

# ╔═╡ a026ca52-4351-41b8-9194-3affc02f370d
num_par = list_input[3]

# ╔═╡ 44f259f7-7039-4865-870f-229ee0dab570
loc_x =loc[1] 

# ╔═╡ 9a292ede-a492-4941-8804-a45ce57c4146
loc_y = loc[2]

# ╔═╡ edf95407-899d-418a-83e6-bed9968e56c2
md"Creating an array"

# ╔═╡ 46517a98-0430-45f6-b196-b49f1cba3151

array = [r * c for r in 1:num_floor, c in 1:num_office]

# ╔═╡ ecdb950c-59a6-4411-89bc-b2dfef224076
md"""# The Problem"""

# ╔═╡ 92639bbd-a021-4b98-9c2b-e85cabf93b41
struct Office
	loc_x::Int64  #make a sure that location matches avail office and floors
	loc_y::Int64
	parcel::Int64
	roomNum
	agent::Bool
end	

# ╔═╡ 3038954f-26f8-409d-9d5e-2b248e1c89d3

office = Office(loc_x,loc_y,array[loc_x,loc_y],"F$loc_x O$loc_y",true)

# ╔═╡ b82cd3f0-df5a-4685-87ed-1e9312d49bc9
struct State
	company::Matrix
	office::Office	
	
end	

# ╔═╡ af906d63-55ba-434c-8ee6-2ae74ca50903
s = State(array,office)

# ╔═╡ baeafa33-104f-46f4-b18f-27643b8bb879
struct Node 
      state::State
	  parent::Union{Node,Nothing}
	  action::Union{Node,Nothing}
end

# ╔═╡ 39c3d87c-758a-4940-8142-fe6ad78828e4
function actions(state)

	possible_actions = ["mw","me","md","mu","co"]
         x = state.office.loc_x  #state.get_location()
         y = state.office.loc_y   
         p = state.office.parcel
	     c = state.company
	
        if x == 1 && possible_actions[1] == "mu"
           if "mu" in possible_actions
            deleteat!(possible_actions, 4)
           end
		elseif y == 1 && possible_actions[2] == "me"
            if "me" in possible_actions
               deleteat!(possible_actions, 2)
	        end
		elseif x == c[end,1:end] && possible_actions[3] == "md"
            if "md" in possible_actions
                deleteat!(possible_actions, 3)
            end
	   elseif y == c[1:end,1] && possible_actions[4] == "mw"
            if "mw" in possible_actions
                deleteat!(possible_actions, 1)
            end
	   elseif p == 0 && possible_actions[5] == "co"
            if "co" in possible_actions
                deleteat!(possible_actions, 5)
            end
        end
        return possible_actions
end

# ╔═╡ 41c87df1-c1f3-4386-a175-7df95348d906
act = actions(s)

# ╔═╡ 4318f698-3a98-4c23-9425-2561d2e6af5f
md" Transition Model"

# ╔═╡ 4b4cc3de-ac19-4c9f-8105-02d5f9e2e9d3
function result(state,action)
 x = state.office.loc_x
 y=  state.office.loc_y
 loc = [[]]

 # Move 
  if action == "mu" 
       loc =[x][y+1]
  elseif action == "md"
      loc = [x][y - 1]
  elseif action == "me"
      loc = [x - 1][y]
  elseif action == "mw"
      loc = [x + 1][y]
  elseif action =="co"
	  loc -=1 
  else
      Print("Invalid action")
  end

  return state
end

# ╔═╡ bc17fb06-09b1-43a4-8712-f6875e1b6626
result(s,act)

# ╔═╡ cb2faab4-576c-4b52-a383-202ea67ee06e
 function  goal_test(state)
	arr = fill(0,loc_x,loc_y)
    return state.company ==   arr

 end

# ╔═╡ 40ce050d-6163-412f-8ada-15d125ef1d99
goal_test(s)

# ╔═╡ 7d039aaa-394a-41a0-9807-96a9b1944119
function h(node::Node)
	
    unit_cost = 0
    new_arr = []
    for i in node.state.company
      if i!= 0
       unit_cost = i * 3
       append!(new_arr,unit_cost)
	  end
	end		
    return new_arr   
	
end

# ╔═╡ b32fa96a-3cbd-4bcd-8245-4afa5a3feaa4
function g(node::Node)
        	
           if node.action == "mw" || node.action == "me"
             
			   return 2
			 
		   elseif  node.action == "mu" || node.action =="md"
            
			   return 1
		   elseif node.action == "co"  
			 
			   return 5
		 
		   else
			   Print("Enter mw,me,mu,md,co")
			
		 
		 end

		 end

# ╔═╡ 3383811f-aa51-42e5-96f9-eae24d2dbe18
function f(node)
   c = g(node)
   h = h(node)
  
	e = c + h
	loc = []
	min(loc)

	 if (a>b): return a
    else: return b
	
end	

# ╔═╡ 13508d93-9070-46d9-98a2-df087375a3b4
function f(e1::Node,e2::Node)
  e1= g(node.action) + h(node.state.company)
  e2 = g(node.action) + h(node.state.company)

	 if e1 < e2
		 return e1
	 elseif e1 == e2 
		 return e1 || e2
	 else 
		 return e2
	 end
end	

# ╔═╡ c375f044-811d-4001-8672-1820796818ca
function get_movement(node)
     movement = [node]
     while !isnothing(node.parent)
     pushfirst!(movement,node)
     end
     return movement
end

# ╔═╡ 48b9ca03-ce58-44b1-b760-05be8373676a
function solution(node, explored)
  loc = get_movement(node)
  for node in loc
      if !isnothing(node.action)
        push!(actions,node.action)
      end
  end

   cost = g(node)
   return cost ,length(node.action)  ,length(explored)
end	

# ╔═╡ 6343fb00-a3f1-44d9-8f6a-b95205066a3f
function aStarAlgorithm(state::State)

    node = Node(state,nothing,nothing)
	if goal_test(node.state) == true
     return solution(node,"Goal",[])
  end

    frontier = PriorityQueue{Node,Int}()
    enqueue!(frontier,node),f(node)
    explored = []
    while frontier
         if isempty(frontier)
           return "no solution found"
         end
          node = dequeue!(frontier)
          push!(explored,node.state)
        for(action,child) in results(node)
        if !(child in keys(frontier)) && !(child.state in explored)
           if goal_test(child.state)
              return solution(child,explored)
           end
           enqueue!(frontier,child,f(child))
       end
    end
	    end
	return None
end
    


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
HypertextLiteral = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
StructArrays = "09ab397b-f2b6-538f-b94a-2f83cf4a842a"

[compat]
DataStructures = "~0.18.11"
HypertextLiteral = "~0.9.3"
PlutoUI = "~0.7.38"
StructArrays = "~0.6.6"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "af92965fb30777147966f58acb05da51c5616b5f"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.3"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataAPI]]
git-tree-sha1 = "fb5f5316dd3fd4c5e7c30a24d50643b73e37cd40"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.10.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "cd56bf18ed715e8b09f06ef8c6b781e6cdc49911"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.4.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.StructArrays]]
deps = ["Adapt", "DataAPI", "StaticArrays", "Tables"]
git-tree-sha1 = "8f705dd141733d79aa2932143af6c6e0b6cea8df"
uuid = "09ab397b-f2b6-538f-b94a-2f83cf4a842a"
version = "0.6.6"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═4ba27e20-cc90-11ec-2be1-077b0b284b2d
# ╠═88e93662-6def-48d3-a58b-5706867e9dc7
# ╠═883d9b0c-2126-4f01-9c84-2fc5c40542f0
# ╠═2e76891f-c5fb-44df-9129-48e90a5e334d
# ╠═585c014d-fcc9-4a39-bc17-23faf9823262
# ╠═bb050b2c-b2c4-41fa-86ee-157735218c85
# ╠═191af719-c27d-45bb-9add-1fa19d9f2bb4
# ╠═4814db00-a3d3-40d8-863e-d4c311cb608b
# ╠═3abbfc16-5b08-4a2d-a54d-20b34fc53d11
# ╠═e283ac78-5272-4c37-ae0c-ed28794e9bd9
# ╠═56d5aa2c-9ed6-40a3-b4e0-eb4378a9b4ef
# ╠═14d02599-537b-419c-ad1b-119735fe515a
# ╠═a026ca52-4351-41b8-9194-3affc02f370d
# ╠═44f259f7-7039-4865-870f-229ee0dab570
# ╠═9a292ede-a492-4941-8804-a45ce57c4146
# ╠═edf95407-899d-418a-83e6-bed9968e56c2
# ╠═46517a98-0430-45f6-b196-b49f1cba3151
# ╠═ecdb950c-59a6-4411-89bc-b2dfef224076
# ╠═92639bbd-a021-4b98-9c2b-e85cabf93b41
# ╠═3038954f-26f8-409d-9d5e-2b248e1c89d3
# ╠═b82cd3f0-df5a-4685-87ed-1e9312d49bc9
# ╠═af906d63-55ba-434c-8ee6-2ae74ca50903
# ╠═baeafa33-104f-46f4-b18f-27643b8bb879
# ╠═39c3d87c-758a-4940-8142-fe6ad78828e4
# ╠═41c87df1-c1f3-4386-a175-7df95348d906
# ╠═4318f698-3a98-4c23-9425-2561d2e6af5f
# ╠═4b4cc3de-ac19-4c9f-8105-02d5f9e2e9d3
# ╠═bc17fb06-09b1-43a4-8712-f6875e1b6626
# ╠═cb2faab4-576c-4b52-a383-202ea67ee06e
# ╠═40ce050d-6163-412f-8ada-15d125ef1d99
# ╠═7d039aaa-394a-41a0-9807-96a9b1944119
# ╠═b32fa96a-3cbd-4bcd-8245-4afa5a3feaa4
# ╠═3383811f-aa51-42e5-96f9-eae24d2dbe18
# ╠═13508d93-9070-46d9-98a2-df087375a3b4
# ╠═c375f044-811d-4001-8672-1820796818ca
# ╠═48b9ca03-ce58-44b1-b760-05be8373676a
# ╠═6343fb00-a3f1-44d9-8f6a-b95205066a3f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
